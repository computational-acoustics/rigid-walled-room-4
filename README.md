# Rigid Walled Room 4

ElmerFEM model for the acoustic eigenmodes of a rigid walled room.

Part of the [accompanying repositories](https://gitlab.com/computational-acoustics) of the [Computational Acoustics with Open Source Software](https://computational-acoustics.gitlab.io/website/) project.

## Covering Episodes

* [Rigid Walled Room Revisited - Part 3](https://computational-acoustics.gitlab.io/website/posts/17-rigid-walled-room-revisited-part-3/).

## Study Summary

The main parameters of the study are reported below.

### Medium Information

|Parameter Name                 | Symbol     | Value | Unit                      |
|-------------------------------|------------|-------|---------------------------|
| Medium Sound Phase Speed      | $`c`$      | 343   | meters per second         |
| Medium Sound Damping          | $`\eta`$   | 0     | squared meters per second |
| Medium Sound Reaction Damping | $`\alpha`$ | 0     | Hertz                     |

### Domain

| Shape            | Size             | Mesh Algorithm  | Mesh Min. Size | Mesh Max. Size   | Element Order |
|------------------|------------------|-----------------|----------------|------------------|---------------|
| Rectangular Room | 5 X 4 X 3 meters | NETGEN 1D-2D-3D | 10 millimetres | 170 millimetres  | Second        |

### Boundary Condition

Rigid walls.

## Software Overview

The table below reports the software used for this project.

| Software                                           | Usage                        |
|----------------------------------------------------|------------------------------|
| [FreeCAD](https://www.freecadweb.org/)             | 3D Modeller                  |
| [Salome Platform](https://www.salome-platform.org) | Pre-processing               |
| [ElmerFEM](http://www.elmerfem.org)                | Multiphysical solver         |
| [ParaView](https://www.paraview.org/)              | Post-processing              |
| [Julia](https://julialang.org/)                    | Technical Computing Language |

## Repo Structure

* `elmerfem` contains the ElmreFEM project.
* `geometry.FCStd` is the FreeCAD geometry model. This file can be used to export geometric entities to _BREP_ files to pre-process with Salome. _BREP_ files are excluded from the repo as they are redundant.
* `meshing.hdf` is the Salome study of the geometry. It contains the geometry pre-processing and meshing. Note that the mesh in this file is **not** computed.
* `validate.jl` contains Julia code to compare the numerical FEM solution to the analytical one. The code will only work only after the study is solved.
* `salomeToElmer` is a submodule containing a script to export a Salome mesh to an ElmerFEM mesh, to be used within Salome.
* The `*.pvsm` files are ParaView state files. They can be loaded into ParaView to plot the solution.

The repo contains only the _source_ of the simulation. To obtain results, the study must be solved.

## How to Run this Study

Follow the steps below to run the study.

Clone the repository and `cd` into the cloned directory:

```bash
git clone https://gitlab.com/computational-acoustics/rigid-walled-room-4.git
cd rigid-walled-room-4/
```

`cd` in the `elmerfem` directory and run the study:

```bash
cd elmerfem/
ElmerSolver
```

To be able to run `validate.jl` as is your Julia environment must have the following packages installed:

* [JLD](https://github.com/JuliaIO/JLD.jl);
* [AcousticModels](https://gitlab.com/computational-acoustics/acousticmodels.jl).
* [FEATools](https://gitlab.com/computational-acoustics/featools.jl).

At this point, you will be able to `cd` back to the repository root and run `validate.jl`:

```bash
cd ../
julia validate.jl
```

When finished, open ParaView and select `File > Load State` to load the `.pvsm` files in the root of the repository. To load the ParaView state successfully, choose _Search files under specified directory_ as shown in the example below. The folder specified in `Data Directory` should be the root folder of the repo (blurred below as the absolute path will differ in your machine).

![Load State Example](res/pictures/paraview-load-state.png)

