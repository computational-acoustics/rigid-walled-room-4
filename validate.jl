#!/usr/bin/env julia

using JLD
using AcousticModels
using FEATools


function validate(
    Lx::Real,
    Ly::Real,
    Lz::Real,
    N::Int
    )
    
    eigen_fem = read_fem_eigenfrequencies("elmerfem/eigenvalues.dat")

    f_min = minimum(eigen_fem)
    if f_min < 1
        f_min = 0
    end
    
    n_modes = length(eigen_fem)

    A, B, C = index_grid(N, N, N)
    F = mode_frequency.(A, B, C, Lx, Ly, Lz)
    eigen_exact = sort(unique(F))
    eigen_exact = eigen_exact[eigen_exact .>= f_min][1:n_modes]

    eigen_error_cents = 1200 * log2.(eigen_fem ./ eigen_exact)
    
    fem_files = readdir("elmerfem/")
    
    vtu_files = joinpath.("elmerfem", fem_files[occursin.("vtu", fem_files)])
    
    signs = zeros(n_modes)
    norms = zeros(n_modes)
    
    for n in 1:n_modes
    
        fem_data = load_mesh(vtu_files[n])
        corrected_data = load_mesh(vtu_files[n])
        exact_data = load_mesh(vtu_files[n])
        check_data = load_mesh(vtu_files[n])
        
        exact_shape = mode.(
            fem_data.points[:, 1],
            fem_data.points[:, 2],
            fem_data.points[:, 3],
            A[F .== eigen_exact[n]][1],
            B[F .== eigen_exact[n]][1],
            C[F .== eigen_exact[n]][1],
            Lx,
            Ly,
            Lz
        )
        
        fem_shape = fem_data.point_data["pressure"]
        
        max_idx = argmax(exact_shape)
        signs[n] = sign(exact_shape[max_idx] / fem_shape[max_idx])
        
        diff_field = signs[n] * fem_shape - exact_shape
        
        norms[n] = sqrt(sum(abs2.(diff_field))) / (Lx * Ly * Lz)
        
        corrected_data.point_data = Dict("pressure" => signs[n] * fem_shape)
        exact_data.point_data = Dict("pressure" => exact_shape)
        check_data.point_data = Dict("error" => diff_field)
        
        write_mesh("elmerfem/corr_t" * lpad(n, 4, "0") * ".vtu", corrected_data)
        write_mesh("elmerfem/exact_t" * lpad(n, 4, "0") * ".vtu", exact_data)
        write_mesh("elmerfem/check_t" * lpad(n, 4, "0") * ".vtu", check_data)
    
    end
    
    return eigen_fem, eigen_exact, eigen_error_cents, signs, norms
    
end


eigen_fem, eigen_exact, eigen_error_cents, signs, norms = validate(5, 4, 3, 100)
save(
    "validation_data.jld",
    "eigen_fem", eigen_fem,
    "eigen_exact", eigen_exact,
    "eigen_error_cents", eigen_error_cents,
    "norms", norms,
    "signs", signs
    )

